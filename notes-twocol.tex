\documentclass[10pt,twocolumn]{article}


\usepackage[utf8]{inputenc}

\usepackage{zsfgv}
\usepackage{lipsum}
\usepackage{multicol}

\begin{document}

\tableofcontents

\section{Foundations}

\subsection{Basics}

\begin{itemize}
\item User has an \textit{Information Need} that we are tying to satisfy
\item Want to identify \textit{relevant documents}.
\end{itemize}

\paragraph{Challenges}
\begin{itemize}
\item How to decide if a document is relevant?
\item How to decide \textit{how} relevant a document is?
\item Finding relevant documents in a large corpus?
\end{itemize}

\paragraph{Basic Approach}  • Go through documents and save statistics on documents
(e.g. \textit{term frequency, document frequency}). ~• Use these to determine
relevance to a query. ~• Can do this at \textit{index time} instead of at query time. 

\paragraph{Basic Definitions}
\begin{itemize}
\item \ild{Document} -- An enclosed ``package'' of information, .e.g. a web
  page, a text file, an article, ...
\item \ild{Collection} -- A set of documents (assumed to be static)
\item \ild{Relevance} -- Does the document satisfy the users information need?
\item \ild{Token} -- Part of text that is considered a single linguistic
  element (i.e. a word or an abbreviation or a number)
\item \ild{Term} -- Equivalence class of tokens as defined by e.g. a common
  stem.
\end{itemize}

\paragraph{\ild{Boolean Retrieval Model}}  We only consider whether a term
appears in a document or not. Hence: • save document-term incidence matrix
(infeasible) ~• Use AND, OR, NOT to search for combinations of query terms.

This is not flexible enough and the incidence matrix is infeasible to
construnct, hence...

\paragraph{\ild{Inverted Index}} Data structure that holds
\begin{itemize}
\item Statistics per each document for each term \\(inverted: Term $\mapsto$ Posting
  List with frequencies)
  \begin{itemize}
  \item \ild{Document frequency}: How many document this term is contained in.
  \item \ild{Term frequency}: How often this term appears in this document
  \end{itemize}
\item Document metadata 
  \begin{itemize}
  \item \ild{Document length}, \ild{average document length}
  \item Name, location on disk, full text, ...
  \end{itemize}
\end{itemize}

\textbf{Data Structures} for inverted index (Dictionary)
\begin{itemize}
\item \textbf{HashMaps}
  \begin{itemize}
  \item Fast lookups
  \item No easy way to find minor variants
  \item No prefix search
  \item Potentially need to rehash
  \item No sorting or sorted sequential access
  \end{itemize}
\item \textbf{Trees} (binary tree, B-Tree)
  \begin{itemize}
  \item Can easily find prefixes (helps with wildcard queries)
  \item Slower lookup
  \end{itemize}
\item Finite State Transducers
\end{itemize}

\paragraph{} The two most basic parts in an IR engine are:
\begin{enumerate}
\item \textbf{Indexing} -- Inverted Index can be constructed iteratively,
  document-by-document.
\item \textbf{Query Processing} -- Retrieve statistics from Inverted Index for
  all query tokens, then rank documents.
\end{enumerate}


\subsection{Indexing}

\paragraph{Basic Steps}
\begin{enumerate}
\item \textbf{Collect Documents},  Register document metadata
\item \textbf{Tokenization} -- equivalence classes can be defined by
  \begin{itemize}
  \item Stemming
  \item Case Folding
  \item Removal of Hyphens, Dots, Apostrophes, ...
  \item Reduction to semantic symbols
  \end{itemize}
\item \textbf{Stop Word Removal}
\item Upsert term into \textbf{Term Dictionary}
\item Upsert term into \textbf{Posting List} for according document (Have a
  mapping from Dictionary Entry to Posting List)
\end{enumerate}

\paragraph{\ild{Tokenization}} Split input string in single words / units of
linguistic information. \textbf{Challenges}:
\begin{itemize}
\item Keep Abbreviations, Dates, Numbers as a single token.
\item Distinguish Abbreviations from words (e.g. \textit{C.A.T.} vs \textit{cat})
\item Apostrophes (\textit{l'hospital} -- one or two tokens?)
\item Hyphenations (\textit{Versicherungs-Gesellschaft, Hewlett-Packard})
\item Names (e.g. TU Wien), \ild{Phrases} (i.e. \textit{Queen of England},
  \textit{San Francisco}, \textit{figure of speech})
\item Compound words
\item Mixture of LTR, RTL text
\item Umlauts, other language-specific things like delimitation of a semantic
  token in chinese or japanese.
\item Mixture of different character sets
\item Handling of synonyms
\end{itemize}

\paragraph{\ild{Stemming}} Reduce tokens to a common root before indexing
(language-specific), e.g. by removing suffixes according to some rules. Tradeoff
between precision and cost.

\paragraph{\ild{Stop Word Removal}}
\begin{itemize}
\item \textbf{Motivation}:  • Smaller data volume  ~• stop words dont have much
  semantic content.
\item \textbf{Challenges}: 
  \begin{itemize}
  \item Good compression techniques mean there is little actual cost for keeping
    stop words.
  \item Needed for • Phrases ~• When semantics are important (e.g.
    \textit{flights to London})
  \end{itemize}
\end{itemize}

\paragraph{\ild{Combination Schemes}} Can improve index by taking into account
phrases that appear very often together (e.g. \textit{TU Wien}, \textit{United Kingdom})

\paragraph{Handling Phrase Queries} Want to be able to search for
\textit{Phrases} like e.g. \textit{TU Wien}. Entered e.g. by putting parts of
query string in quotes. Possible approaches:
\begin{enumerate}
\item \textbf{Biword indexes} -- Index by not one token but pairs. Can then
  query longer phrases by AND on pairs. 
  \begin{itemize}
  \item Can have false positives (potentially returns more than wanted)!
  \item Much larger index
  \end{itemize}
\item \textbf{Positional Indexes} -- • For each term, store the position it
  appears in the file (token index). ~ • Get doc:pos lists for each term in
  phrase from dictionary ~ • merge these by document ~ • then find the ones
  where terms are succeeding (or other, for proximity search)
  \begin{itemize}
  \item Larger index size
  \end{itemize}
\end{enumerate}

\paragraph{Handling Wildcard Queries} Approaches
\begin{itemize}
\item For a query \textit{foo*qux}, find results for \textit{foo*} and
  separately for \textit{*qux} (\ild{maintain backwards B-Tree}), then intersect
  results and use positional indices to check. (Need multiple trees)
\item \ild{Permuterm Index}: Index each term in seperate entries (e.g.
  \textit{hello} goes to \textit{helloX}, \textit{elloXh}, \textit{lloXhe}, ...)
  where X is a special character. ~  • Then rotate query term so that wildcard
  is on end. $\rightarrow$ larger index size.
\item \ild{$k$-gram indices} Keep mapping from $k$-grams of characters to
  dictionary terms that match each $k$-gram, then \textit{mon*} can be queried
  by \textit{Xm} AND \textit{mo} AND \textit{on}
  \begin{itemize}
  \item Again, false positives -- must filter again against queries
  \item Less expensive compared to permuterm approach
  \end{itemize}
\end{itemize}

\paragraph{Spelling Correction} Can apply to indexing or query processing.
\textit{Basic assumption:} Have a dictionary with ``correct'' spellings (could
also be just all terms of corpus). \textit{Objective}: Find the word in the
dictionary that is ``closest'' to the entered word. Distance measures:
\begin{itemize}
\item \textbf{(weighted) edit distance} -- given a query, enumerate all strings within a
  given edit distance of query term, intersect these with correct words from
  dictionary. Show these as suggestions. Calculating the edit distance to every
  dictionary term is expensive -- use...
\item \textbf{$n$-gram overlap} -- keep $n$-gram index, then consider all words with a
  certain number of overlapping $n$-grams as alternatives (as score, use
  \textit{Jaccard coefficient})
\item \textbf{Context-sensitive for phrase queries} -- Fix one word at a time, suggest the alternative with
  the most hits.
\end{itemize}

\paragraph{Soundex} reduce tokens into phonetic equivalents.


\section{Efficient Text Processing}

Ideas:
\begin{itemize}
\item Avoid garbage collection (e.g  • use array pool ~  • manage memory
  manually)
\item Avoid manipulations on strings (substring, split, ...) -- Strings are
  immutable, so this causes the creation and allocation of new objects.
\item Use memory-mapped files.
\item Avoid creating objects and work directly with byte buffers instead.
\item Use int/long when possible
\item Interpret strings (charbuffers) as longs for comparison (e.g. in stop word
  removal)
\item Use multithreading
\item Measure!
\end{itemize}


\section{Scoring Documents}

\subsection{Basics}

Disadvantages of Boolean Search (retrieve all documents matching boolean query)
\begin{itemize}
\item Often result in too many or too few results
\item Writing queries with the intended breadth can be hard \\
  {\footnotesize AND produces high precision but low recall; OR gives low
    prevision but high recall}
\item Output is not really ranked
\item All terms are equally weighted
\end{itemize}

Assume \ild{free text queries}, that is: query is not a semantically string
combination of operators and expression but just a string. 

\paragraph{\ild{Bag of Words}}: Idea that order of terms is not important for
ranking documents.

\paragraph{\ild{Term Frequency $tf(t,d)$}} How often does this term appear in
this document? Commonly used in combination with a logarithm: tf-weight $=
\log(1+ tf(t,d))$. Logarithm has kind of a dampening effect (extremely large
values are not as large).

\paragraph{\ild{Document Frequency $df(t)$}} How many documents does this term
appear in?

Idea: Rare terms are more informative than frequent terms -- should have a high
score. Hence inverse fraction and use log-dampening: \ild{Inverse document
  frequency $idf(t)$}$=\log(\frac{n}{df(t)})$

\paragraph{\ild{tf-idf weight}}
\begin{align*}
  \mathit{tf.idf}(t,d) = \log(1+tf(t,d)) \cdot \log(\frac{n}{df(t)})
\end{align*}
\begin{itemize}
\item $tf(t,d)$ increases with the number of terms in the document
\item $idf(t)$ increases with the rarity of the term in the collection
\end{itemize}

We define the score of a document $d$ w.r.t a query $q$:
\begin{align*}
  \mathit{score}(q,d) = \sum_{t \in q \cap d} \mathit{tf.idf(t,d)}
\end{align*}

\textbf{Problem}: It's a sum: Longer document $\rightarrow$ Potentially higher
term frequency in document $\rightarrow$ Higher score

\subsection{Vector Space Model}

\paragraph{\ild{Vector Space model}}
\begin{itemize}
\item Each document is represented a vector with a component for each term.
\item Can use $tf.idf(t,d)$ as weight for a component.
\item Query is also considered a Vector
\item Need a distance function
  \begin{itemize}
  \item Euclidean distance is bad because: It's a sum of differences of
    components. Thus the most relevant document would be one that is exactly
    equal to the query. Any longer document would receive a worse score, even if
    it still only contained query terms. 
  \item Use \ild{cosine similarity} instead (angle between vectors).
  \end{itemize}
\end{itemize}

\paragraph{Performance considerations}
\begin{itemize}
\item Only want to find top $k$ documents, not order entire collection.
\item General approach: Identify set of candidates of which most are in top $k$.
  Returning the top $k$ of \textit{these} is assumed to suffice.
\item Only consider
  \begin{itemize}
  \item docs containing at least one query term
  \item docs containing many query terms
  \item high-idf query terms (i.e. rare terms, do not consider e.g. stop words,
    those appear in many documents)
  \end{itemize}
\item Consider static quality score $g(d)$ (authority) -- citations, bookmarks,
  PageRank, ...; use this in computation of score (e.g. linear combination with relevance)
\item Order all postings by $g$ (static, common ordering, independent of query).
  Then, top-scoring documents are likely to appear earlier in postings traversal.
\item In time-bound applications, we can stop traversing early.
\end{itemize}

\subsection{Probabilistic Model}

We are working with semantically imprecise items, i.e. different terms mean the
same thing, different documents are relevant to some degree to a query, etc. Can
we try to use mathematical probability to model this uncertainty?

$\rightarrow$ \textit{Probability of relevance to query} $P(\mathit{rel} ~|~ d,
q)$. Rank by decreasing probability of relevance.

\subsubsection{Binary Independence Model}

\paragraph{Assumptions}
\begin{itemize}
\item Binary term-document incidence vectors
\item Assume independence between terms
\item Relevance of document is independent of relevance of other documents
\item Assuming that relevant documents are a small fraction of the collection,
  statistics for nonrelevant documents can be approximated by statistics for the
  entire collection.
\end{itemize}

\subsubsection{BM25}

Maybe check IR book?

\subsection{Language Model}

Based on the contents of a document, come up with an abstract (generative) model
of it. Then, compute the probability that the query was generated by this model.

Take a finite state automaton as a deterministic language model. For simplicity,
assume a unigram language model, i.e. a FSA with a single state. Transitions
from/to that state generate different words with different probability.

Under term independence, the probability that a query string is generated is then
the product of the probabilities of its terms.

$P(q ~|~ d)$ is the probability that $q$ is generated by $d$. From Bayes' Rule
we have
\begin{align*}
  P(d ~|~ q) = \frac{P(q~|~d) P(d)}{P(q)}
\end{align*}
where $P(q)$ is independent of document (ignored), $P(d)$ is assumed to be the
same for all $d$. So, we can equivalently rank according to $P(q~|~d)$ (the
probability that a query $q$ would be observed as a random sample from the document
model of $d$, called $M_d$).

Under independence assumption, we have
$$P(q~|~M_d) = \prod P(q_i~|~M_d)$$
which is equivalent to
$$P(q~|~M_d) = \prod_{t \in q} P(t~|~M_d)^{\mathit{tf}(t,q)} $$
(only need to figure out probabilities for occurence of single terms).

Now need to figure out $P(t~|~M_d)$, i.e. the probability that a (single) term
$t$ is generated by $M_d$. Can estimate with $\cPr{t}{M_d} \approx
\frac{\mathit{tf}(t,d)}{\abs{d}}$.

Problem: Need to avoid zeroes in product. Assume a non-occuring term has the
non-zero probability equal to occuring by chance in the collection model $M_c$:
$$ \cPr{t}{M_c} = \frac{\mathit{cf(t)}}{T} $$
where $T$ is total number of tokens in collection. --- Then, use this as a
linear combination with $\cPr{t}{M_d}$.
\begin{align*}
  \cPr{t}{d} = \lambda \cPr{t}{M_d} + (1-\lambda) \cPr{t}{M_c}
\end{align*}
\begin{itemize}
\item $\lambda$ large, more focus on document model: Tends to retrieve documents
  containing all query words (because value of product must be high).
\item $\lambda$ small, more focus on collection model: More weight if term is
  popular in collection, independent of document to be ranked.
\end{itemize}

Summary (\ild{Mixture Model}):
\begin{align*}
  \cPr{q}{d} \approx \prod_{q_i \in q} \lambda \cPr{q_i}{M_d} + (1-\lambda) \cPr{q_i}{M_c}
\end{align*}


\section{Evaluation}

\paragraph{Motivation}
\begin{itemize}
\item Need to find reliable comparison measures for IR systems
\item Large amounts of documents
\item Human subjectivity involved (judge relevance)
\end{itemize}

Kinds of evaluation:
\begin{itemize}
\item Efficiency (technical concerns, time and space)
\item Effectiveness (Quality of results, harder to measure)
\item User studies
\item \ild{intrinsic} (consider and measure retrieved set) vs. \ild{extrinsic}
  (evaluate in entire usage-context)
\end{itemize}

Things to measure (Cleverdon)
\begin{itemize}
\item Coverage -- How well can system cover my needs?
\item Time lag
\item Presentation
\item User effort required
\item Recall
\item Precision
\end{itemize}

\paragraph{\ild{Precision}} How accurate is the result that we received?
\begin{align*}
  \text{Precision} = \frac{\text{relevant retrieved docs}}{\text{total \textit{retrieved} docs}}
\end{align*}

\paragraph{\ild{Recall}} How much of the available information did we actually retrieve?
\begin{align*}
  \text{Recall} = \frac{\text{relevant retrieved docs}}{\text{total \textit{relevant} docs}}
\end{align*}


$\rightarrow$ \ild{F-Measure} combines both into a single measure. Loses
expressive power though. 

\paragraph{\ild{Precision-Recall curve}}
A precision-recall curve shows the relationship between precision (= positive predictive value) and recall (= sensitivity) for every possible cut-off.

With increasing allowed size of retrieved set, plot Precision vs. Recall.

With increasing size, recall will most likely improve and precision will fall.

\paragraph{Construction}
\begin{enumerate}
\item Define cutoffs of results size
\item For each cutoff, for each query, measure precision and recall
\end{enumerate}

\paragraph{Methods}
\begin{itemize}
\item Transform into step function by taking as value the maximum of previously
  found precisions.
\item Average points from different queries / rankings
\end{itemize}

\paragraph{Challenges}
\begin{itemize}
\item \textbf{Graded Relevance} -- Doesn't take into account the order of the
  retrieved documents and that some documents could in fact be more relevant
  than others
\item \textbf{Cutoff Value} -- Both $P$ and $R$ are related to some result set
  size, i.e. a cutoff value. 
\end{itemize}

\subsection{Handling Cutoff Value}
Different approaches:
\paragraph{\ild{$r$-Precision}} Fix cutoff value at number of relevant documents.
Precision at the $r$-th position where $r$ is the number of relevant documents.

\paragraph{\ild{Reciprocal Rank}}
\begin{align*}
  RR = \frac{1}{\text{rank of first relevant document}}
\end{align*}

\paragraph{\ild{Mean Average Precision}} Average precision at ranks of relevant documents.
\begin{align*}
 AP = \frac{\sum P(i) \cdot rel(i)}{R}
\end{align*}
where $R$ is number of relevant documents, $i$ is the rank, $k$ number of
retrieved documents, $P(i)$ precision at rank and $rel(i)$ boolean indicator if
$i$ is relevant. --- Then take mean of this over all queries.

\subsection{Handling graded relevance}

\paragraph{\ild{Cumulative Gain}} Let $\text{rel}(d,q)$ be a nonnegative (and nonbinary!) 
relevance score for $i$. Then
\begin{align*}
  CG = \sum_i rel(i,q)
\end{align*}
Problem: How to judge relevance?

\paragraph{\ild{Discounted Cumulative Gain}} Gain is accumulated starting at the
top, Gain summands diminish with larger index by scaling down with log. Thus, a
system that returns more relevant documents at a higher rank receives a higher
score. 

\textbf{Problem}: Both CG and DCG depend on the number of relevant documents per topic! So, they
cannot be used to compare performance across topics! --- Can normalise by
dividing with \ild{Ideal DCG} (value for optimal return set).

\paragraph{\ild{Rank-biased precision}} Alternative to CG variants.
\begin{align*}
  RBP(n) &:= (1-p) \cdot \sum_{i} \mathit{rel}(i) \cdot p^{i-1} \\
  &= \sum_{i} \mathit{rel}(i) \cdot (1-p) \cdot p^{i-1}
\end{align*}
$p$ can be see as the probability of moving to the next result.


\subsection{Properties of effectiveness metrics}

\begin{enumerate}
\item \textbf{Boundedness}
\item \textbf{Monotonicity}
\item \textbf{Convergence} -- score decreases if a relevant document is replaced
  with a non-reported less-relevant document.
\item \textbf{Top-weightedness} -- score decreases if a relevant document is swapp with a
  reported less-relevant document.
\item \textbf{Localization} -- score for top $k$ can be computed using only the top $k$ results.
\item \textbf{Completeness} -- A score can be calculated even if the query has
  no relevant documents.
\item \textbf{Realizability} -- provided that the collection has at least one relevant
  document, it is possible for the score at depth $k$ to be maximal.
\end{enumerate}

\subsection{Statistical Validity}

Naturally, all experimental results must be statistically meaningful. We test
against the null hypothesis that there is no difference between some two
systems.

\paragraph{\ild{p-Value}} Likelihood that we observe a result based on the
distribution implied by the null hypothesis (as such ``just a coincidence'').
Commonly assumed that hypothesis is valid if $p <= 0.05$.

\textbf{Increasing statistical validity} -- By increasing the number of topics
covered by the system. Then, we can feel more confident that average scores and
therefore their differences between systems are meaningful/significant.

\subsection{Determining Relevance}

Ideal: Manual assession of all documents. \textbf{Problem}: Highly subjective,
highly different results between different judges (Agreement rarely above $80\%$)

Infeasible because collections are very large. Instead: Only look at the sets of
returned documents by the systems that are tested. \textbf{Assumption}: If a
document is not returned by any tested system it is surely not relevant.

\paragraph{\ild{Pooling}} Use union of top $k$ documents from each run as
\ild{pool}, give this to humans for relevance assessment.

Advantages:
\begin{itemize}
\item Fewer documents have to be manually assessed
\end{itemize}
Disadvantages:
\begin{itemize}
\item Have to pick size of pool ($k$) adequately so that meaningful but still
  feasible. Usually $k=100$.
\item Gives no real statement about how many documents have been found at all
  (some might not have been returned by any tested system)
\item If a system does find a relevant document but ranks it lower than $k$, it
  is not taken into account.
\end{itemize}

Solve some of these problems by...
\paragraph{\ild{Pooling with randomized sampling}} Add to the pool some
documents that are randomly sampled from the entire retrieved set (Use
stratified sampling to get more higher-ranking ones)

\paragraph{Problem}: Have to take into account that some documents are not
ranked (not manually assessed).

\paragraph{\ild{BPref}} Measure for the quality of a ranking, takes unjudged
documents into account. Summe über jd. relevante Dokument $r$: Anteil von
Inversionen in Ergebnisliste bis zu $r$. Das Ganze geteilt durch Gesamtzahl
relevanter Dokumente.
\begin{align*}
  \mathit{bpref} = \frac{1}{R} \sum_r (1- \frac{\abs{n \text{ ranked higher than } r}}{\min(R,N)} )
\end{align*}


\subsubsection{Designing User-based Evaluations}
Can either measure  • some relevance score based on user assessments or  • user
satisfaction. Will mostly talk about first point.

\paragraph{Evaluating relevance of queries} -- Approaches
\begin{itemize}
\item Rate for each document if it is relevant to given query
\item Given two documents, rate which is more relevant
  \begin{itemize}
  \item More assessments needed but
  \item Better inter-annotator agreement
  \end{itemize}
\item Let user judge the entire list of results as a whole
  \begin{itemize}
  \item How to present this in experiment (two-panel view is limiting, maybe
    interspersed list with click monitoring)
  \end{itemize}
\end{itemize}

\paragraph{Discussion}
\begin{itemize}
\item Doubts about the expressivity of \textit{Recall}, \textit{MAP},
  \textit{Cumulative Gain}.
\item Have to design experiments properly (realistic tasks)
\item Other kinds of relevance factors than simply relevance to topic? Also want
  • diversity  • quality  • credibility  • ease of access

\end{itemize}


\section{Web Search}
\subsection{Characteristics}
\begin{itemize}
\item Large amounts of available information
\item Network is highly dynamic
\item Not managed or curated
\item Different kinds of tasks:  • Information  • Navigation  • Action  • Exploration
\item People will actively try to cheat (link farms, hidden text, cloaking,
  article spinning, ...)
\end{itemize}

\subsection{Crawling}
\begin{enumerate}
\item Start from set of seed pages
\item Extract URLs they point to
\item Place these on a queue to continue with in the next step
\end{enumerate}

A crawler should be
\begin{itemize}
\item Polite: Avoid hitting a site too often, respect explicit constraints (robots.txt)
\item Robust: Be immune to spider traps, handle spam pages well
\item Distributed
\item Scalable
\item Performant/Efficient
\item Quality-aware
\item Fresness-aware: Do hit previously seen pages again to update information
  (tradeoff with Politeness)
\end{itemize}

\subsection{Ranking}
\subsubsection{Basics on Link Analysis}
\paragraph{Assumptions}
\begin{itemize}
\item A hyperlink to page is a signal of quality for that page
\item The anchor text (text surrounding the hyperlink) describes the content of the page
\item[$\rightarrow$] A link may describe the page better than the page itself
\end{itemize}

\paragraph{Usefulness of anchor text} For each page, also index anchor texts
pointing to it.
\begin{itemize}
\item Taking these frequencies into account too may increase query accuracy (IBM
  example)
\item Anchor text may contain useful related terms (e.g. synonyms, nicknames)
  associated to the page.
\end{itemize}

\subsubsection{PageRank} See contents from GrDa

\paragraph{Clarification on handling Dead Ends} In the matrix, a dead end is a
column with only zeroes. We replace these with $\frac{1}{n}$ (random
teleport to any other page).

\paragraph{Shortcomings}
\begin{itemize}
\item Measures generic popularity of a page -- Biased against topic-specific
  authorities.
\item Susceptible to link spam -- articially link topographies (spam farms)
\item Uses a single model/idea of importance
\item Random Surfer model might not be adequate to reality (bookmarks, back
  button, search, ...)
\end{itemize}


\subsection{Query Log Analysis}

\paragraph{Objectives} • enhance effectivess/efficiency of system  • enhance user experience

Distribution of query popularity follows a Power Law.

Queries can be of different user intent:
\begin{itemize}
\item Information
\item Navigation
\item (Trans-)Action
\end{itemize}

Search happens in \textit{sessions}

Users often repeat searches.

\paragraph{Use Cases}
\begin{itemize}
\item User experience
  \begin{itemize}
  \item Query Expansion (e.g. \textit{fix iphone} $\rightarrow$ \textit{fix
      iphone repair ...})
  \item Query Suggestion -- provide list of queries related to current query;
    autocomplete
  \item Spelling correction
  \item Use logs as training data for ranking algorithms
  \end{itemize}
\item Efficiency -- exploit usage information to cache/position/partition data
  accordingly in the system

\end{itemize}
\paragraph{Challenges}
\begin{itemize}
\item (Clicks can be taken as a quality measure, however) users do not always want
  results that correspond to their profile.
\end{itemize}





\section{Search Interfaces}
\paragraph{\ild{Usability}} Five components
\begin{enumerate}
\item \textbf{Learnability} -- How easy is it to accomplish a task for the first time?
\item \textbf{Efficiency} -- Cost/Utility ratio
\item \textbf{Memorability} -- How long does it take to re-establish proficiency
  after a period of non-use
\item \textbf{Errors} -- How many errors does the user make, how easy is it to recover?
\item \textbf{Satisfaction}
\end{enumerate}

\paragraph{\ild{Design Guidelines for Search Interfaces}}
\begin{itemize}
\item \textbf{Offer informative feedback}
\begin{itemize}
  \item Show search results immediately (without intermediary step)
  \item Show document surrogates, highlight query terms
  \item Allow sorting of results by various criteria
  \item Show query term suggestions (\textit{related term suggestion},
    \textit{term expansion})
  \item Use explicit relevance indicators sparingly
  \item Support high responsitivity of system
\end{itemize}
\item \textbf{Balance user control with automated actions}
  \begin{itemize}
  \item e.g. automatic incorporation of similar/related terms in query
  \end{itemize}
\item \textbf{Reduce short-term memory load} 
  \begin{itemize}
  \item History mechanisms
  \item Display information in a structured way, e.g. hierarchies, categories,
    \ild{faceted metadata} (e.g. person characteristics), clustering
  \end{itemize}
\item \textbf{Provide shortcuts}
  \begin{itemize}
  \item Deep links (direct links to subpages in surrogate)
  \item Widgets on result page (e.g. time of day, calculator, election results, ...)
  \end{itemize}
\item \textbf{Reduce errors}
  \begin{itemize}
  \item Spelling corrections
  \item Avoid completely empty result sets -- perform query expansion, ...
  \end{itemize}
\item \textbf{Recognise the importance of details} 
  \begin{itemize}
  \item Size/Shape of query text input field
  \item Positioning of spelling corrections
  \item ...
  \end{itemize}
\item \textbf{Recognise the importance of aesthetics} 
\end{itemize}




\section{Music Retrieval}

missing

\section{Übungsfragen}
\textit{Beschrieben Sie die Qualitätsmaße \textit{Precision} und \textit{Recall} sowie
deren Tradeoff.}
\begin{itemize}
\item \textbf{Precision}: Anteil von zurückgegebenen relevanten Dokumenten an allen
  zurückgegeben Dokumenten.
\item \textbf{Recall}: Anteil von zurückgebenen relevanten Dokumenten an
  \textit{allen relevanten} Dokumenten.
\item \textit{Tradeoff}: Optimierung von Recall (gebe mehr hoffentlich relevante
  Dokumente zurück) führt potentiell zu Fehlern (gebe doch irrelevantes Dokument
  zurück), was wiederum Precision negativ beeinflusst. Optimierung von Precision
  (schränke zurückgegebene Dokumente besser ein) führt potentiell zu Fehlern
  (gebe manche relevante Elemente nicht zurück), was wiederum Recall
  beeinflusst. 
\end{itemize}


\textit{Beschreiben Sie unterschiedliche Methoden zur Feature Space Reduction
  bei Textanalyse-Aufgaben}
\begin{itemize}
\item Case Folding
\item Stemming
\item Stop Word Removal
\item Remove Rare Terms
\end{itemize}

\textit{Beschreiben Sie unterschiedliche Methoden bzw. Arten von Feature Spaces,
die bei der Indizierung von Textdokumenten zum Einsatz kommen}
\begin{itemize}
\item Bag-of-Words -- $\rightarrow$ binary independence model
\item $n$-grams
\item word stems
\end{itemize}

\textit{Beschreiben Sie die tfxidf-Methode zur Gewichtung von Termen, wie diese
  Maße berechent werden und welche Annahmen dahinter stehen.}

\begin{itemize}
\item \textit{Term frequency}: Anzahl Vorkommen von Term in Document
\item \textit{Document frequency}: Anzahl von Dokumenten, in denen Term
  vorkommt. Nutze log-dampening.
\item \textit{Inverse document frequency}: Nutze dieses Maß, um seltene Terme
  höher zu gewichten: $idf(t) = \log(\frac{n}{df(t)})$ (\textbf{Annahme})
\item Damit ist das tfxidf-Maß definiert durch 
  \begin{align*}
    \mathit{tf.idf}(t,d) = \log(1+tf(t,d)) \cdot idf(t)
  \end{align*}
\item \textbf{Annahmen}:
  \begin{itemize}
  \item Binary Independence Model (Terme sind unabhängig voneinander, lediglich
    Vorkommen eines Terms in Dokument zählen, Dokumente sind unabhängig voneinander)
  \item Allgemein (in Korpus) seltene Terme sind von höherer Ausschlagskraft für die Gewichtung.
  \item Anteil (Frequency) von Term an Dokument ist Indikator für Wichtigkeit.
  \end{itemize}
\end{itemize}

\textit{Weshalb ist term frequency allein ungeeignet für das Ranking?}
\begin{itemize}
\item \textbf{Lange Dokumente} sind mit vielen Vorkommen desselben Terms liefern eine
  höhere \textit{term frequency} obwohl nicht unbedingt aussagekräftiger/wichtiger --
  absolutes Maß, abhängig von Länge des Dokuments.
\item \textbf{Sehr oft vorkommende Terme} (zB Artikel) erhalten hohe \textit{term
    frequency}, sind aber aufgrund Ihrer hohen Häufigkeit im gesamten Korpus
  nicht aussagekräftig -- Nehme deshalb \textit{inverse document frequency}
  (niedrig, wenn Term hohe \textit{df} hat).
\end{itemize}



\end{document}